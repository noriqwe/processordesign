-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- ***************************************************************************
-- This file contains a Vhdl test bench template that is freely editable to   
-- suit user's needs .Comments are provided in each section to help the user  
-- fill out necessary details.                                                
-- ***************************************************************************
-- Generated on "03/29/2016 15:19:25"
                                                            
-- Vhdl Test Bench template for design  :  Control_Unit
-- 
-- Simulation tool : ModelSim (VHDL)
-- 

LIBRARY ieee;                                               
USE ieee.std_logic_1164.all;                                

ENTITY Control_Unit_vhd_tst IS
END Control_Unit_vhd_tst;
ARCHITECTURE Control_Unit_arch OF Control_Unit_vhd_tst IS
-- constants                                                 
-- signals                                                   
SIGNAL ALUOP : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL ALUSrcA : STD_LOGIC;
SIGNAL ALUSrcB : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL AM : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL clk : STD_LOGIC;
SIGNAL IROPwrite : STD_LOGIC;
SIGNAL IRwrite : STD_LOGIC;
SIGNAL MemRead : STD_LOGIC;
SIGNAL MemToReg : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL opcode : STD_LOGIC_VECTOR(5 DOWNTO 0);
SIGNAL PCsrc : STD_LOGIC_VECTOR(1 DOWNTO 0);
SIGNAL PCwrite : STD_LOGIC;
SIGNAL RegWrite : STD_LOGIC;
SIGNAL reset : STD_LOGIC;
SIGNAL success : STD_LOGIC_VECTOR(7 DOWNTO 0);
COMPONENT Control_Unit
	PORT (
	ALUOP : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	ALUSrcA : OUT STD_LOGIC;
	ALUSrcB : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	AM : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
	clk : IN STD_LOGIC;
	IROPwrite : OUT STD_LOGIC;
	IRwrite : OUT STD_LOGIC;
	MemRead : OUT STD_LOGIC;
	MemToReg : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	opcode : IN STD_LOGIC_VECTOR(5 DOWNTO 0);
	PCsrc : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
	PCwrite : OUT STD_LOGIC;
	RegWrite : OUT STD_LOGIC;
	reset : IN STD_LOGIC;
	success : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	);
	
END COMPONENT;
BEGIN
	i1 : Control_Unit
	PORT MAP (
-- list connections between master ports and signals
	ALUOP => ALUOP,
	ALUSrcA => ALUSrcA,
	ALUSrcB => ALUSrcB,
	AM => AM,
	clk => clk,
	IROPwrite => IROPwrite,
	IRwrite => IRwrite,
	MemRead => MemRead,
	MemToReg => MemToReg,
	opcode => opcode,
	PCsrc => PCsrc,
	PCwrite => PCwrite,
	RegWrite => RegWrite,
	reset => reset,
	success => success
	);
	
	AM <= "01";
	opcode <= "111000";
	reset <= '1', '0' after 2 ns;
	
clk_gen : PROCESS
begin
  loop
    clk <= '1'; wait for 5 ns;
    clk <= '0'; wait for 5 ns;
  end loop;
end PROCESS clk_gen;

init : PROCESS                                               
-- variable declarations                                     
BEGIN                                                        
        -- code that executes only once                      
WAIT;                                                       
END PROCESS init;                                           
always : PROCESS                                              
-- optional sensitivity list                                  
-- (        )                                                 
-- variable declarations                                      
BEGIN                                                         
        -- code executes for every event on sensitivity list  
WAIT;                                                        
END PROCESS always;                                          
END Control_Unit_arch;
