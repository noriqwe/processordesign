library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ProgramMemory is
  port(
    address      : in  std_logic_vector(15 downto 0);
	 readData        : out std_logic_vector(15 downto 0)
    );
end ProgramMemory;


architecture arch of ProgramMemory is
type dataMemory is array(0 to 3) of std_logic_vector(15 downto 0);
signal registers : dataMemory :=
(
"0000000000000000",
"0000000000010001",
"0000000000100010",
"0000000000110011"
);
  
  
  
  
begin
readData <= registers(to_integer(unsigned(address)));

end arch;

